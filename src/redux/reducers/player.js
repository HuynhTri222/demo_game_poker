let initialState = {
  playerList: [
    {
      username: "Player 1",
      totalPoint: 0,
      cards: [],
    },
    {
      username: "Player 2",
      totalPoint: 0,
      cards: [],
    },
    {
      username: "Player 3",
      totalPoint: 0,
      cards: [],
    },
    {
      username: "your name",
      totalPoint: 0,
      cards: [],
    },
  ],
};

const reducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case "SET_NAME":
      {
        // let temp = {...state.playerList[3]};
        // temp.username = payload;
        state.playerList[3].username = payload;
        return {...state};
      }

    default:
      return state;
  }
};

export default reducer;
