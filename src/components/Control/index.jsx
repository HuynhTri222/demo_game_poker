import React from "react";
import { useSelector } from "react-redux";

const Control = () => {

  const playerList = useSelector((state) => state.player.playerList);

  const renderPlayer = () => {
    return playerList.map((player, index) => (
      <div key={index} className="border px-3 text-center">
      <p> {player.username} </p>
      <p> {player.totalPoint} </p>
    </div>
    ));
  }

  return (
    <div className="d-flex  justify-content-end container">
      <div className="border d-flex justify-content-center align-items-center px-2">
        <button className="btn btn-success mr-2">Shuffle</button>
        <button className="btn btn-info mr-2">Draw</button>
        <button className="btn btn-primary mr-2">Reveal</button>
      </div>
      <div className="d-flex">   
        {renderPlayer()}   
      </div>
    </div>
  );
};

export default Control;
