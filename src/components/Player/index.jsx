import React from "react";
import { useSelector } from "react-redux";
import Card from "../Card";

const Player = (props) => {

  const renderCardList = () => {
    return props.player.cards.map((card, index) => (
      <Card key={index} card={card}/>
    ))
  }

  return (
    <div className={`player-${props.index}`}>
      <p className="lead">{props.player.username}</p>
      <main className="d-flex">
        {
          renderCardList()
        }
      </main>
    </div>
  );
};

export default Player;
