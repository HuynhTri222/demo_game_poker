import React, { useState } from "react";
import Game from "../Game";
import { useDispatch } from "react-redux";

const Home = () => {

  const [gameOn, setGameOn] = useState(false);
  const [name, setName] = useState("");

  const dispatch = useDispatch();

  const handleChange = (e) => {
    const {value} = e.target;
    setName(value);
  }

  const handleGameOn = () => {
    // Co che ghi de, khac voi class la merge, beb dung spread operator
    setGameOn(true);
    dispatch({
      type: "SET_NAME",
      payload: name,
    });
  }


  return (
    <>
      {/* {
        gameOn ? */}
          <Game />
          {/* : */}
          <div
            className="text-center"
            style={{
              width: "100vw",
              height: "100vh",
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            <h1 className="diplay-4 mb-5"> Welcome to Pocker Center</h1>
            <h3>Fill your name and start</h3>
            <input onChange={handleChange} type="input" className="w-25 form-control mb-3" />
            <button onClick={handleGameOn} className="btn btn-success">Start new Game</button>
          </div>
      {/* } */}


    </>
  );
};

export default Home;
