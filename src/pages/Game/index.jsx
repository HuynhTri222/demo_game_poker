//tuuong nhu pure component, memo cung sai shallow compare
import React, { memo, Fragment } from "react";
import "./index.css";
import Controls from "../../components/Control";
import Main from "../../components/Main";

const Game = () => {
  
  console.log("Game render");
    return (
      <Fragment>
        <Controls />
        <Main />
      </Fragment>
    );

}

export default memo(Game);
